package fr.yekta.gsb_frais.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fr.yekta.gsb_frais.R;
import fr.yekta.gsb_frais.models.Client;

/**
 * Created by ASUS on 08/04/2017.
 */

public class ClientAdapter extends ArrayAdapter<Client> {

    // Une liste de client
    private ArrayList<Client> listeClient;

    //Le contexte dans lequel est présent notre adapter
    private Context mContext;
    private int mResource;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater mInflater;

    /**
     * Constructeur par défaut
     * Générer automatiquement
     * @param context
     * @param resource
     * @param lesClients
     */

    public ClientAdapter(Context context, int resource, ArrayList<Client> lesClients) {
        super(context, resource, lesClients);
        mResource = resource;
        this.listeClient = listeClient;
    }


    public View getView(int position, View convertView, ViewGroup parent){

        // Get the data item for this position
        Client client = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(mResource, parent, false);
        }

        // Lookup view for data population
        TextView tvNomPrenom = (TextView) convertView.findViewById(R.id.textView1);
        TextView tvAdresse = (TextView) convertView.findViewById(R.id.textView2);
        TextView tvCodePostalVille = (TextView) convertView.findViewById(R.id.textView3);
        TextView tvTelephone = (TextView) convertView.findViewById(R.id.textView4);

        // Populate the data into the template view using the data object
        tvNomPrenom.setText(client.getNom().toUpperCase() + " " + client.getPrenom());
        tvAdresse.setText(client.getAdresse());
        tvCodePostalVille.setText(client.getCodePostal() + " " + client.getVille().toUpperCase());
        tvTelephone.setText(client.getTelephone());
        // Return the completed view to render on screen
        return convertView;
    }
}
