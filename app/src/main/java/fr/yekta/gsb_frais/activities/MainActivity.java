package fr.yekta.gsb_frais.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import fr.yekta.gsb_frais.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Bouton "Lieux De Visite" qui renvoie vers la page des listes des lieux de visite ( Les Clients ).
    public void btnListeLieux(View view) {
        Intent intent = new Intent(this, ClientActivity.class);
        startActivity(intent);
    }

    // Bouton "Lieux De Visite" qui renvoie vers la page des listes des lieux de visite ( Les Clients ).
    public void btnListeVisite(View view) {
        Intent intent = new Intent(this, VisiteActivity.class);
        startActivity(intent);
    }
}
