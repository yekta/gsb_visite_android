package fr.yekta.gsb_frais.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import fr.yekta.gsb_frais.R;
import fr.yekta.gsb_frais.adapters.ClientAdapter;
import fr.yekta.gsb_frais.models.Client;
import fr.yekta.gsb_frais.models.DatabaseHelper;


public class ClientActivity extends AppCompatActivity {

    ArrayList<Client> listeClient = new ArrayList<Client>();

    DatabaseHelper gsbDB ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_lieux);

        gsbDB = new DatabaseHelper(this);

        //Log.d("Local", String.valueOf(listeClient));

        //Appel de la méthode qui charge et affiche les clients
        afficherLesClients();


        //Bouton flottant pour un ajouter un nouveau lieu de visite (client)
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_client);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            //Affichage du formulaire pour ajouter un nouveau lieu de visite (client)
            popupAjoutClient();
            }
        });
    }

    /**
     * Méthode qui charge et affiche la liste des clients
     */
    public void afficherLesClients(){

        listeClient = gsbDB.getAllClient();

        //Création et initialisation de l'Adapter pour les personnes
        final ClientAdapter adapter = new ClientAdapter(this, R.layout.content_liste_lieux, listeClient);
        //Récupération du composant ListView
        final ListView list = (ListView)findViewById(R.id.listView);
        //Initialisation de la liste avec les données
        list.setAdapter(adapter);

        //list.setSelection(adapter.getCount()-1);

        //Lorsque l'on clique sur un client
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Client unClient = (Client) parent.getItemAtPosition(position);

            popupChoix(unClient, adapter);
            }
        });
    }


    //Méthode qui affiche un formulaire popup
    //Formulaire d'ajout d'un lieu de visite
    public void popupAjoutClient(){

        AlertDialog.Builder popupAjouterClient = new AlertDialog.Builder(ClientActivity.this);
        AlertDialog contenuAjouterClient = popupAjouterClient.create();
        View vueContenuAjouterClient = getLayoutInflater().inflate(R.layout.content_ajouter_client, null);

        contenuAjouterClient.setView(vueContenuAjouterClient);
        contenuAjouterClient.show();

        ajouterClient(vueContenuAjouterClient, contenuAjouterClient);
    }

    //Formulaire ajouter un client
    public void ajouterClient(final View uneVue, final AlertDialog contenuAjouterClient){

        final TextView nomInput = (TextView) uneVue.findViewById(R.id.nomInput);
        final TextView prenomInput = (TextView) uneVue.findViewById(R.id.prenomInput);
        final TextView adresseInput = (TextView) uneVue.findViewById(R.id.adresseInput);
        final TextView codepInput = (TextView) uneVue.findViewById(R.id.codepInput);
        final TextView villeInput = (TextView) uneVue.findViewById(R.id.villeInput);
        final TextView telephoneInput = (TextView) uneVue.findViewById(R.id.telephoneInput);

        final Button btnAjout = (Button) uneVue.findViewById(R.id.btnAjout);

        btnAjout.setOnClickListener(new View.OnClickListener()  {

            @Override
            public void onClick(View v) {

                String sNom = nomInput.getText().toString();
                String sPrenom = prenomInput.getText().toString();
                String sAdresse = adresseInput.getText().toString();
                String sCodep = codepInput.getText().toString();
                String sVille = villeInput.getText().toString();
                String sTel = telephoneInput.getText().toString();

                if (sNom.matches("") || sPrenom.matches("") || sAdresse.matches("") || sCodep.matches("") || sVille.matches("") || sTel.matches("")) {
                    Toast.makeText(ClientActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_LONG).show();
                    return;
                }

                //Insertion d'un nouveau client qui retourne un booleen true ou false.
                boolean resultatInsertion = gsbDB.ajouterUnClient(sNom, sPrenom, sAdresse, sCodep, sVille, sTel);

                //Si le resultat est true
                if(resultatInsertion == true){
                    //Message Toast
                    Toast.makeText(ClientActivity.this,
                            "Un nouveau lieu a été ajouté",
                            Toast.LENGTH_SHORT).show();

                    //Met à jour la liste des clients.
                    afficherLesClients();
                    //Fermer la boite de dialogue
                    contenuAjouterClient.dismiss();
                }else{

                    //Message Toast
                    Toast.makeText(ClientActivity.this,
                            "ERREUR D'INSERTION",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Affiche une Popup "supprimer" ou "modifier" un client
     */
    public void popupChoix(final Client unClient, final ClientAdapter adapter){

        AlertDialog.Builder popupChoix = new AlertDialog.Builder(ClientActivity.this);
        final AlertDialog contenuChoix = popupChoix.create();
        View vueContenuChoix = getLayoutInflater().inflate(R.layout.content_choix, null);

        contenuChoix.setView(vueContenuChoix);
        contenuChoix.show();

        Button btnSupp = (Button) contenuChoix.findViewById(R.id.btnSupprimer);
        Button btnModif = (Button) contenuChoix.findViewById(R.id.btnModifier);

        //Lorsque le bouton Supprimer est cliqué
        btnSupp.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View v) {
                //Appel méthode pour supprimer un client
                supprimerClient(unClient, adapter);
                //Ferme le PopUp
                contenuChoix.dismiss();
                //Message Toast
                Toast.makeText(ClientActivity.this, unClient.getNom() + " a été supprimé de la liste",Toast.LENGTH_SHORT).show();
            }
        });

        //Lorsque le bouton Modifier est cliqué
        btnModif.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View v) {
                //Ferme le PopUp
                contenuChoix.dismiss();

                popupModifierClient(unClient, adapter);
            }
        });
    }

    public void popupModifierClient(Client unClient, ClientAdapter adapter){

        AlertDialog.Builder popupModifierClient = new AlertDialog.Builder(ClientActivity.this);
        final AlertDialog contenuModifierClient = popupModifierClient.create();
        View vueModifierClient = getLayoutInflater().inflate(R.layout.content_modifier_client, null);

        contenuModifierClient.setView(vueModifierClient);
        contenuModifierClient.show();

        modifierClient(vueModifierClient, contenuModifierClient, unClient, adapter);
    }

    public void modifierClient(final View uneVue, final AlertDialog contenuModifierClient, final Client unClient, final ClientAdapter adapter){

        final TextView nomInput = (TextView) uneVue.findViewById(R.id.nomInput);
        nomInput.setText(unClient.getNom());

        final TextView prenomInput = (TextView) uneVue.findViewById(R.id.prenomInput);
        prenomInput.setText(unClient.getPrenom());

        final TextView adresseInput = (TextView) uneVue.findViewById(R.id.adresseInput);
        adresseInput.setText(unClient.getAdresse());

        final TextView codepInput = (TextView) uneVue.findViewById(R.id.codepInput);
        codepInput.setText(unClient.getCodePostal());

        final TextView villeInput = (TextView) uneVue.findViewById(R.id.villeInput);
        villeInput.setText(unClient.getVille());

        final TextView telephoneInput = (TextView) uneVue.findViewById(R.id.telephoneInput);
        telephoneInput.setText(unClient.getTelephone());

        final Button btnModif = (Button) uneVue.findViewById(R.id.btnUpdate);
        btnModif.setTag(unClient.getId());

        btnModif.setOnClickListener(new View.OnClickListener()  {

            @Override
            public void onClick(View v) {

                String sNom = nomInput.getText().toString();
                String sPrenom = prenomInput.getText().toString();
                String sAdresse = adresseInput.getText().toString();
                String sCodep = codepInput.getText().toString();
                String sVille = villeInput.getText().toString();
                String sTel = telephoneInput.getText().toString();

                if (sNom.matches("") || sPrenom.matches("") || sAdresse.matches("") || sCodep.matches("") || sVille.matches("") || sTel.matches("")) {
                    Toast.makeText(ClientActivity.this, "Veuillez remplir tous les champs", Toast.LENGTH_LONG).show();
                    return;
                }

                //Insertion d'un nouveau client qui retourne un booleen true ou false.
                boolean resultatInsertion = gsbDB.updateUnClient((Integer) v.getTag(), sNom, sPrenom, sAdresse, sCodep, sVille, sTel);

                //Si le resultat est true
                if(resultatInsertion == true){
                    //Message Toast
                    Toast.makeText(ClientActivity.this,
                            "Le client a été ajouté",
                            Toast.LENGTH_SHORT).show();

                    //Met à jour du client
                    unClient.setNom(sNom);
                    unClient.setPrenom(sPrenom);
                    unClient.setAdresse(sAdresse);
                    unClient.setCodePostal(sCodep);
                    unClient.setVille(sVille);
                    unClient.setTelephone(sTel);

                    adapter.notifyDataSetChanged();

                    //Fermer la boite de dialogue
                    contenuModifierClient.dismiss();
                }else{

                    //Message Toast
                    Toast.makeText(ClientActivity.this,
                            "ERREUR D'INSERTION",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void supprimerClient(Client unClient, ClientAdapter adapter){
        //Supression du client selectionner
        listeClient.remove(unClient);
        gsbDB.deleteUnClient(unClient.getId());
        //Mise à jour de la vue
        adapter.notifyDataSetChanged();
    }




}
