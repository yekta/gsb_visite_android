package fr.yekta.gsb_frais.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import fr.yekta.gsb_frais.R;
import fr.yekta.gsb_frais.models.Client;
import fr.yekta.gsb_frais.models.Visite;

/**
 * Created by ASUS on 03/06/2017.
 */

public class VisiteAdapter extends ArrayAdapter<Visite> {

    // Une liste de visite
    private ArrayList<Visite> lesVisites;

    //Le contexte dans lequel est présent notre adapter
    private Context mContext;
    private int mResource;

    //Un mécanisme pour gérer l'affichage graphique depuis un layout XML
    private LayoutInflater mInflater;

    /**
     * Constructeur par défaut
     * Générer automatiquement
     * @param context
     * @param resource
     * @param lesVisites
     */
    public VisiteAdapter(Context context, int resource, ArrayList<Visite> lesVisites) {
        super(context, resource, lesVisites);
        mResource = resource;
        this.lesVisites = lesVisites;
    }


    public View getView(int position, View convertView, ViewGroup parent){

        // Get the data item for this position
        Visite visite = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(mResource, parent, false);
        }

        // Lookup view for data population
        TextView tvDate = (TextView) convertView.findViewById(R.id.textView1);
        TextView tvNomPrenom = (TextView) convertView.findViewById(R.id.textView2);
        TextView tvCodePostalVille = (TextView) convertView.findViewById(R.id.textView3);
        TextView tvDistance = (TextView) convertView.findViewById(R.id.textView4);
        //TextView tvAdresse = (TextView) convertView.findViewById(R.id.textView3);

        // Populate the data into the template view using the data object
        tvDate.setText(visite.getDate());
        tvNomPrenom.setText(visite.getUnClient().getNom().toUpperCase()+ " " + visite.getUnClient().getPrenom());
        tvCodePostalVille.setText(visite.getUnClient().getCodePostal() + " " + visite.getUnClient().getVille().toUpperCase());
        tvDistance.setText(visite.getDistance() +" km parcourue");


        // Return the completed view to render on screen
        return convertView;
    }
}
