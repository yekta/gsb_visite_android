package fr.yekta.gsb_frais.models;

/**
 * Created by ASUS on 08/04/2017.
 */

public class Visite {

    private int id;
    private String date;
    private int distance;
    Client unClient;

    public Visite(int id, String date, int distance, Client unClient) {
        this.id = id;
        this.date = date;
        this.distance = distance;
        this.unClient = unClient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public Client getUnClient() {
        return unClient;
    }

    public void setUnClient(Client uneClient) {
        this.unClient = unClient;
    }

    @Override
    public String toString() {
        return "Visite{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", distance=" + distance +
                ", uneClient=" + unClient +
                '}';
    }
}
