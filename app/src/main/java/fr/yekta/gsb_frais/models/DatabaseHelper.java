package fr.yekta.gsb_frais.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by ASUS on 03/06/2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "GSB.db" ;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        SQLiteDatabase db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE client (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, prenom TEXT, adresse TEXT, code_postal TEXT, ville TEXT, telephone TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS client (id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, prenom TEXT, adresse TEXT, code_postal TEXT, ville TEXT, telephone TEXT)");
        onCreate(db);
    }

    public boolean ajouterUnClient(String nom, String prenom, String adresse, String code_postal, String ville, String telephone) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("nom", nom);
        contentValues.put("prenom", prenom);
        contentValues.put("adresse", adresse);
        contentValues.put("code_postal", code_postal);
        contentValues.put("ville", ville);
        contentValues.put("telephone", telephone);

        long result = db.insert("client", null, contentValues);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }

    public ArrayList<Client> getAllClient(){

        ArrayList<Client> result = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM client", null);

        while (res.moveToNext()){

            int id = res.getInt(0);
            String nom = res.getString(1);
            String prenom = res.getString(2);
            String adresse = res.getString(3);
            String codePostal = res.getString(4);
            String ville = res.getString(5);
            String telephone = res.getString(6);

            Client cl = new Client(id, nom, prenom, adresse, codePostal, ville, telephone);

            result.add(cl);

        }

        return result;
    }

    //Delete Query
    public void deleteUnClient(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "DELETE FROM client WHERE ID = " + id;
        db.execSQL(sql);
    }

    //Update Query
    public boolean updateUnClient(int id, String nom, String prenom, String adresse, String codePostal, String ville, String telephone) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("nom", nom);
        contentValues.put("prenom", prenom);
        contentValues.put("adresse", adresse);
        contentValues.put("code_postal", codePostal);
        contentValues.put("ville", ville);
        contentValues.put("telephone", telephone);

        long result = db.update("client", contentValues, "id="+id, null);

        //String sql = "UPDATE "+ TABLE_NAME +" SET NOM = '"+ nom +"', PRENOM = '"+ prenom +"', ADRESSE =  '"+ adresse +"', CODE_POSTAL =  '"+ codePostal +"', VILLE =  '"+ ville +"', TELEPHONE =  '"+ telephone +"' WHERE ID = " + id;
        //db.execSQL(sql);

        if(result == -1){
            return false;
        }else{
            return true;
        }
    }


}