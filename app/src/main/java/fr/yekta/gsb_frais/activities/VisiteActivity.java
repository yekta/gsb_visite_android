package fr.yekta.gsb_frais.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import fr.yekta.gsb_frais.R;
import fr.yekta.gsb_frais.adapters.ClientAdapter;
import fr.yekta.gsb_frais.adapters.VisiteAdapter;
import fr.yekta.gsb_frais.models.Client;
import fr.yekta.gsb_frais.models.Visite;

public class VisiteActivity extends AppCompatActivity {

    ArrayList<Visite> lesVisites = new ArrayList<Visite>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_visite);

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        lesVisites.add(new Visite(1, "03/06/2017", 47, new Client(1, "CIRPAN1", "Yekta1", "34 rue victor hugo", "59540", "Caudry", "07 86 79 10 10")));
        lesVisites.add(new Visite(2, "02/06/2017", 71, new Client(1, "CIRPAN2", "Yekta1", "34 rue victor hugo", "59540", "Caudry", "07 86 79 10 10")));
        lesVisites.add(new Visite(3, "01/06/2017", 103, new Client(1, "CIRPAN3", "Yekta1", "34 rue victor hugo", "59540", "Caudry", "07 86 79 10 10")));
        lesVisites.add(new Visite(4, "25/05/2017", 17, new Client(1, "CIRPAN4", "Yekta1", "34 rue victor hugo", "59540", "Caudry", "07 86 79 10 10")));

        afficherListeVisite();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab_visite);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void afficherListeVisite(){
        //Création et initialisation de l'Adapter pour les personnes
        final VisiteAdapter adapter = new VisiteAdapter(this, R.layout.content_liste_lieux, lesVisites);
        //Récupération du composant ListView
        final ListView list = (ListView)findViewById(R.id.listViewVisite);
        //Initialisation de la liste avec les données
        list.setAdapter(adapter);

        //Lorsque l'on clique sur un client
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Visite uneVisite = (Visite) parent.getItemAtPosition(position);

                //popupChoix(unClient, adapter);

                //afficherLesClients();

            }
        });
    }



}
