package fr.yekta.gsb_frais;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    public boolean nbEntier(int Nombre) {
        boolean result = false;
        if(Nombre > 0 && Nombre == (int)Nombre ){
            result = true;
        }
        return result;
    }

    @Test
    public void testNbEntier()  throws Exception {
        assertEquals(nbEntier(5), true);
        assertEquals(nbEntier(-5), false);
    }
}